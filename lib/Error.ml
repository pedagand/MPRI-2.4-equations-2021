module Signature = struct
  type 'a t = exn

  let map _ s = s

end

module Error = Free.Make (Signature)
include Error

let err (e:exn) : 'a t =
   op e

let run (m:'a t) =
  let algebra =
    let return x = x in
    let op = raise in
    {return;op}
  in run algebra m
